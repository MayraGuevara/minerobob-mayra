class Tavern constructor(val name: String, var thrist: Int) {
    fun Print() {
        println(
            """
            $name: Hombre, estoy sediento. Me dirijo a la taberna.
            $name: Oh! Esto sí que está muy bueno, ha calmado mi sed.
        """.trimIndent()
        )
    }

    fun Drink() {
        thrist = thrist - 5
        if (thrist < 0) {
            thrist = 0
        }

        Print()
    }
}