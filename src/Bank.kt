class Bank constructor(val name: String, var gold: Int, var bank: Int) {
    fun Print() {
        println(
            """
            $name: Me voy de la mina de oro con mis bolsillos llenos.
            $name: Voy para el banco, sí señor.
            $name: Depositando el oro en el banco. Total de oro ahorrado: $bank.
        """.trimIndent()
        )
    }

    fun DepositGold() {

        bank = bank + gold

        Print()
        gold = 0
    }
}