class Home constructor(val name: String, var fatige: Int) {
    fun Print() {
        println(
            """
            $name: Me siento muy cansado. Iré a descansar.
            $name: De camino a mi hogar dulce hogar
            $name: Hogar dulce hogar.
            $name: Zzzzz…
        """.trimIndent()
        )
    }

    fun Sleep() {
        fatige = 0
        Print()
    }
}